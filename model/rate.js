const db = require('../common/db');
let rate = function() {};

rate.graph = {};

rate.prototype.getLatest = function(){
  let sql = `select i.datetime,i.exchange,i.source,i.destination,i.forward,i.backward from input_rate i
    inner join (
    select max(datetime) as max_datetime,exchange,source,destination from input_rate
    group by exchange,source,destination ) temp on i.datetime = temp.max_datetime and i.exchange = temp.exchange and i.source = temp.source and i.destination = temp.destination
    group by i.exchange,i.source,i.destination`;

  return db.query(sql);
}

rate.prototype.log = function(rate){
  let log_sql = `insert into input_rate set ?`;
  return db.query(log_sql,[rate]);
}

rate.prototype.update = function() {
  return this.getLatest()
  .then(rs=>{
    if(rs && rs.length > 0){
      let updates = rs.reduce((accu,row)=>{
        let rate = row;
        let sql = `insert into rate(??) values(?) on duplicate key update ?`;
        accu.push(db.query(sql,[
          ['datetime','exchange','source','destination','forward','backward'],
          [rate.datetime,rate.exchange,rate.source,rate.destination,rate.forward,rate.backward],
          {
            datetime:rate.datetime,
            forward:rate.forward,
            backward:rate.backward
          }
        ]));
        return accu;

      },[]);
      return Promise.all(updates);
    }else{
      new Error('rate not found');
    }
  })
}

rate.prototype.get = function(params){
  let {exchange,source,destination} = params;

  let sql = `select * from rate where exchange=? and source=? and destination=?`;
  return db.query(sql,[exchange,source,destination]);
}

rate.prototype.initGraph = function(){
  let sql = `select * from rate`;
  let V = [];
  let rate = {};
  let next = {};
  return db.query(sql)
  .then(records=>{
    let size = records.length * 2;
    for(let i = 0; i < size; i++){
      for(let j = 0; j < size; j++){
        if(rate[i]){
          rate[i][j] = 0;
        }else{
          rate[i] = {
            [j]:0
          }
        }
        if(next[i]){
          next[i][j] = null;
        }else{
          next[i] = {
            [j]:null
          }
        }
      }
    }
    let i = 0;
    records.forEach(record=>{
      let sourceVertexIndex = i;
      //init V
      V.push({
        index:sourceVertexIndex,
        exchange:record.exchange,
        currency:record.source
      });
      i++;
      let destinationVertexIndex = i;
      V.push({
        index:destinationVertexIndex,
        exchange:record.exchange,
        currency:record.destination
      })
      i++;
      rate[sourceVertexIndex][destinationVertexIndex] = record.forward;
      rate[destinationVertexIndex][sourceVertexIndex] = record.backward;
    })
    V.forEach(v=>{
      let currency = v.currency;
      let index = v.index;
      V.filter(_v=>(_v.currency == currency && _v.index != index))
      .forEach(__v=>{
        rate[index][__v.index] = 1;
        rate[__v.index][index] = 1;
      })
    })
    for(let i = 0; i < size; i++){
      for(let j = 0; j < size; j++){
          next[i][j] = V.filter(v=>(v.index == j))[0];
      }
    }

    let graph = {
      V:V,
      rate:rate,
      next:next
    }
    //console.log(graph);
    this.graph = graph;
    return graph
  })
}

rate.prototype.bestRate = function(){

  let {V,rate,next} = this.graph;
  //console.log(V);

  for(let k=0; k < V.length;k++){
    for(let i=0; i < V.length;i++){
      for(let j=0; j < V.length; j++){
        if(!rate[i][j]){
          rate[i][j] = 0
        }
        if(!rate[i][k]){
          rate[i][k] = 0
        }
        if(!rate[k][j]){
          rate[k][j] = 0
        }
        if(i != j && i != k && k != j){
          if(rate[i][j] < (rate[i][k] * rate[k][j])){
            //console.log(rate);
            //console.log(i + '->' + k + '->' + j)
            //console.log(rate[i][j] + ' = ' + rate[i][k] + ' x ' + rate[k][j])
            rate[i][j] = rate[i][k] * rate[k][j]
            //console.log(rate[i][j] + ' = ' + rate[i][k] + ' x ' + rate[k][j])
            //console.log(" ");
            next[i][j] = next[i][k]
          }
        }
      }
    }
  }
  //console.log('best rate path');
  //console.log(next);
  return rate;
}

rate.prototype.path = function(u,v){
  let {V,rate,next} = this.graph;
  //console.log('path path');
  //console.log(next);
  if(next[u][v] == null){
    return [];
  }
  let path = [u];
  while (u !== v){
    let nextVetor = next[u][v];
    //console.log("next vector: " + u + "->" + v)
    u = nextVetor.index;
    path.push(u);
  }
  return path;
}

module.exports = new rate();

const Rate = require('./model/rate');

Rate.initGraph()
.then(graph=>{
  let source_exchange = process.argv[2];
  let source_currency = process.argv[3];
  let dest_exchange = process.argv[4];
  let dest_currency = process.argv[5];
  let {V} = graph;
  if(source_exchange && source_currency && dest_exchange && dest_currency){
    let u = null;
    let v = null;
    try{
      u = V.filter(v=>(v.exchange == source_exchange && v.currency == source_currency))[0].index;
    }catch(e){
      throw "Invalid source exchange or source currency";
    }
    try{
      v = V.filter(v=>(v.exchange == dest_exchange && v.currency == dest_currency))[0].index;
    }catch(e){
      throw "Invalid destination exchange or destination currency";
    }
    let rate = Rate.bestRate();
    let path = Rate.path(u,v);
    console.log("BEST_RATES_BEGIN");
    console.log(source_exchange + " " + source_currency + " " + dest_exchange + " " + dest_currency + " " + rate[u][v]);
    path.forEach(p=>{
      let vertex = V.filter(v=>(v.index == p))[0];
      console.log(vertex.exchange + " " + vertex.currency);
    })
    process.exit();
  }else{
    throw "Invalid arguments. Expecting <source_exchange> <source_currency> <destination_exchange> <destination_currency>";
  }

})
.catch(err=>{
  console.log(err);
  process.exit()
})

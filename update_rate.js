const rate_parser = require('./common/rate_parser');
const Rate = require('./model/rate');

rate_parser.then(chunk=>{
  return chunk.reduce((promise,r)=>{
    return promise
    .then(res=>{
      return Rate.log(r);
    })
  },Promise.resolve());
})
.then(rs=>{
  console.log("Start updating exchange rate....");
  Rate.update();
  console.log("Rates updated");
  process.exit()
})
.catch(err=>{
  console.log(err);
  process.exit()
})

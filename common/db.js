const mysql = require('mysql');


let rate = function() {};


const pool = mysql.createPool({
  "host": "52.76.37.15",
  "user": "root",
  "password": "CAguilera",
  "database": "playground"
});

pool.query = function(query, values) {
  //console.log("start query");
  //console.log(query);
  return new Promise((fulfill, reject) => {
    pool.getConnection((error, connection) => {
      if (error) {
        reject(error);
      }
      var t0 = process.hrtime();
      var res = connection.query(query, values, (error, results, fields) => {
        connection.release();
        if (error) {
          reject(error);
        }
        else {
          var diff = process.hrtime(t0);
          var ms = diff[0]*1e3 + diff[1]/1e6;
          /*
            console.log(res.sql)
            console.log('--------------------')
            console.log('affectedRows = '+(results.affectedRows || results.length || 0))
            console.log('\x1b[33m%s\x1b[0m', 'lapsed = '+ms+'ms');
            console.log('--------------------')
            */
          fulfill(results);
        }
      })
      //console.log(res.sql)
    })
  })
}



module.exports = pool;

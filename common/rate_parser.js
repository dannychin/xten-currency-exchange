const inputFile='./input.csv';
const fs = require('fs');
let p = new Promise((resolve,reject)=>{
  let readStream = fs.createReadStream(inputFile,'utf8');
  let chunks = [];
  readStream.on('data', chunk => {
        let data = chunk.split("\n");
        chunks = data.reduce((accu,line)=>{
          let line_data = line.split(" ");
          let rate = {
            datetime:line_data[0],
            exchange:line_data[1],
            source:line_data[2],
            destination:line_data[3],
            forward:line_data[4],
            backward:line_data[5]
          }
          if(line_data[0]){//make sure it is not empty
            accu.push(rate);
          }
          return accu
        },[])
        //chunks.push(rates);
  }).on('end', function () {
        //console.log('All the data in the file has been read');
        //console.log(chunks);
        resolve(chunks);
  }).on('error', function(err){ /*handle error*/
        reject(err);
  });
})

module.exports = p;

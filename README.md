### What is this repository for? ###
TenX Code test
To solve to problem:
1. update exchange rate
2. get the best exchange rate and path

### System Requirement ###
1. Nodejs v6.4
2. Mysql database (Please recover the database structure from TenX.sql)

### How To Update the exchange rate ###
I assume the rates will be updating using a CSV file. 
I created a job update_rate.js to read the CSV file and update the rate in the database.It will read the updated rate form the file input.csv

Run the following command to update the exchange rates:

node update_rate.js 


### How To Get the best exchange rate ###
Just run the following command to get the best rate:

node get_rate.js <source_exchange> <source_currency> <destination_exchange> <destination_currency>

Example if you want to get best rate from <GDAX,BTC> to <KRAKEN,USD>

node get_rate.js GDAX BTC KRAKEN USD

### Remeber to update the rate first before you try to get the best exchange rate ###

